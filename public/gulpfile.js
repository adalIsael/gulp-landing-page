var gulp = require("gulp");
var imagemin = require('gulp-imagemin');
var cleanCss = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var js = require('gulp-uglify');
var runSequence = require('run-sequence');

gulp.task('minificar-imagenes', function(){
  gulp.src('src/images/*')
  .pipe( imagemin())
  .pipe( gulp.dest("public/images"));
});

gulp.task('minificar-css', function(){
  gulp.src('src/css/*.css')
    .pipe(cleanCss())
    .pipe(autoprefixer())
    .pipe( gulp.dest("public/css"));
});

gulp.task('minificar-js', function(){
  gulp.src('src/js/*')
    .pipe( js())
    .pipe( gulp.dest("public/js"));
});

var browserSync = require('browser-sync');

gulp.task('browser-sync' function(){
var config = {
  startPatch: "index.html",
  notify: false,
  server: {
    baseDir: 'public/'
  }
};
  browserSync.init(config);
});

gulp.task('browser-reload', function(){
  browserSync.reload();
});

//workflow: $ gulp
gulp.task('default', function(){
  runSequence(
    ['browser-sync'],
    ['minificar-imagenes'],
    ['minificar-css'],
    ['minificar-js'],
  );

  gulp.watch(['src/*', 'src/**/*'], function(){
    runSequence(
      ['minificar-imagenes'],
      ['minificar-css'],
      ['minificar-js'],
      ['browser-reload']
    );
  });
});
